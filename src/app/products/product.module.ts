import { NgModule } from '@angular/core';

import { Router, RouterModule } from '@angular/router';

import { ProductDetailComponent } from './product-detail.component';
import { ProductListComponent } from './product-list.component';

import { ProductService } from './product.service';
import { ProductGaurdService } from './product-gaurd.service';

import { ConvertToSpacePipe } from './../shared/convert-to-spaces.pipe';
import { SharedModule } from './../shared/shared.module';


@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'products', component: ProductListComponent },
      { path: 'products/:id', canActivate: [ProductGaurdService], component: ProductDetailComponent }
    ]),
    SharedModule
  ],
  declarations: [
    ProductDetailComponent,
    ProductListComponent,
    ConvertToSpacePipe,
  ],
  providers: [
    ProductService,
    ProductGaurdService
  ]
})
export class ProductModule { }
